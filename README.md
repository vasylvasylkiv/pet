# Project Installation
1. Install [ddev](https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/).
2. Run `ddev start` to spin up the project

## Import database
- Run `ddev drush sql-cli < ./db/database-init.sql` to import init database.
