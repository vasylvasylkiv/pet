<?php

function petkit_form_system_theme_settings_alter(&$form, Drupal\Core\Form\FormStateInterface $form_state) {
  $form['custom_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Petkit Theme Settings'),
    '#collapsible' => true,
    '#collapsed' => true,
  );

  $form['custom_settings']['tabs']['theme_floating'] = array(
    '#type' => 'select',
    '#title' => t('Floating label'),
    '#default_value' => theme_get_setting('theme_floating'),
    '#options'  => array(
      'default' => t('Default'),
      'yes' => t('Yes'),
    ),
  );
}
