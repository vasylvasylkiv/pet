<?php

/**
 * @file
 * Main file for custom theme hook preprocess.
 */

use Drupal\node\NodeInterface;

/**
 * Implements hook_preprocess_HOOK() for custom-module-random-posts.html.twig.
 */
function template_preprocess_custom_module_random_posts(array &$variables) {
  $items = [];

  /** @var \Drupal\node\NodeInterface $node */
  $node = \Drupal::routeMatch()->getParameter('node');
  if ($node instanceof NodeInterface) {
    /** @var \Drupal\node\NodeStorageInterface $node_storage */
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    /** @var \Drupal\node\NodeViewBuilder $node_view_builder */
    $node_view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');
    /** @var \Drupal\custom_module\Service\CustomModuleManagerInterface $custom_module_manager */
    $custom_module_manager = \Drupal::service('custom_module.manager');

    $random_post_ids = $custom_module_manager->getRandomPosts(4, [$node->id()]);

    foreach ($random_post_ids as $id) {
      /** @var \Drupal\node\NodeInterface $random_post */
      $random_post = $node_storage->load($id);
      $items[] = $node_view_builder->view($random_post, 'teaser');
    }
    $user_name = \Drupal::currentUser()->getAccountName();
  }

  $variables['items'] = $items;
  $variables['user_name'] = $user_name;
}
