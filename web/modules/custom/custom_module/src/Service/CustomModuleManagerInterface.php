<?php

namespace Drupal\custom_module\Service;

use Drupal\node\NodeInterface;

/**
 * Custom Module Manager Interface.
 *
 */
interface CustomModuleManagerInterface {

  /**
   * Gets random article posts.
   *
   * @param int $limit
   *   The max limit of posts.
   * @param array $exclude_ids
   *   The array with node id's which must be excluded.
   *
   * @return array
   *   The random posts entity id's.
   */
  public function getRandomPosts($limit = 2, array $exclude_ids = []);



}
