<?php

namespace Drupal\custom_module\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;

/**
 * Helpers for articles.
 *
 */
class CustomModuleManager implements CustomModuleManagerInterface {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The node view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $nodeViewBuilder;

  /**
   * CustomModuleManager constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->nodeViewBuilder = $entity_type_manager->getViewBuilder('node');
  }


  /**
  * {@inheritdoc}
  */
  public function getRandomPosts($limit = 2, array $exclude_ids = []) {
    $query = $this->nodeStorage->getQuery()->accessCheck()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', 'article')
      ->range(0, $limit);

    if (!empty($exclude_ids)) {
    $query->condition('nid', $exclude_ids, 'NOT IN');
    }

    $query->addTag('entity_query_random');

    return $query->execute();
  }
}
